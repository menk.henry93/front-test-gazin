import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify'
import mixins from './mixins'
import '@babel/polyfill'
import '@/styles/global.css'

Vue.config.productionTip = false
Vue.mixin(mixins)

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
