function copy(item) {
  let copyObject = {}
  let key
  for (key in item) {
    copyObject[key] = item[key]
  }
  return copyObject
}

export default {
  copy
}
