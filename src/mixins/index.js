import store from '@/store/index'
export default {
  data() {
    return {
      listSex: [
        {
          id: 'M',
          description: 'Masculino'
        },
        {
          id: 'F',
          description: 'Feminino'
        },
        {
          id: 'O',
          description: 'Outros'
        }
      ]
    }
  },

  methods: {
    async createAlert(color, message) {
      await store.dispatch('actionOpenSnackbar', {
        text: message,
        color: color
      })
    }
  }
}
