import config from '../config/connection'
const PATH = 'developers/'

export const findAll = params => {
  const query = `developers?page=${params.page}&itemsPerPage=${params.itemsPerPage}&findName=${params.findName}`

  return config()
    .get(query)
    .then(res => res.data)
}

export const findOne = id => {
  return config()
    .get(PATH + id)
    .then(res => res.data)
}

export const create = input => {
  return config()
    .post(PATH, input)
    .then(res => res.data)
}

export const edit = input => {
  return config()
    .put(PATH + input._id, input)
    .then(res => res.data)
}

export const remove = id => {
  return config()
    .delete(PATH + id)
    .then(res => res.data)
}
