import { TRIGGER_SNACKBAR, INACTIVE_SNACKBAR, TRIGGER_LOADER } from './types'

const state = {
  snackbar: {
    active: false,
    text: '',
    color: ''
  },
  loader: false
}

const getters = {}

const actions = {
  actionOpenSnackbar({ commit }, data) {
    commit('TRIGGER_SNACKBAR', data)
  },
  actionCloseSnackbar({ commit }) {
    commit('INACTIVE_SNACKBAR')
  },
  actionTriggerLoader({ commit }, estado) {
    commit('TRIGGER_LOADER', estado)
  }
}

const mutations = {
  [TRIGGER_SNACKBAR](state, data) {
    const { text, color } = data
    state.snackbar.active = true
    state.snackbar.text = text
    state.snackbar.color = color
  },

  [INACTIVE_SNACKBAR](state) {
    state.snackbar.active = false
  },

  [TRIGGER_LOADER](state, estado) {
    state.loader = estado
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
