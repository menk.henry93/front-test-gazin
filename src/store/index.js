import { createStore } from 'vuex-extensions'
import createPersistedState from 'vuex-persistedstate'
import developers from './developers'
import global from './global'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default createStore(Vuex.Store, {
  plugins: [createPersistedState()],
  modules: { global, developers }
})
