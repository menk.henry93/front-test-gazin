import { ALL_DEVELOPERS, GET_ONE_DEVELOPER } from './types'
import { findAll, findOne, create, edit, remove } from '../../service/developer'
const state = {
  developers: [],
  developer: {}
}

const getters = {
  getDevelopers: state => state.developers,
  getDevelopersPagination: state => state.developersPagination,
  getDeveloper: state => state.developer
}

const actions = {
  async actionFindAll({ commit, dispatch }, config) {
    dispatch('actionTriggerLoader', true)
    await findAll(config).then(res => {
      commit('ALL_DEVELOPERS', res)
      dispatch('actionTriggerLoader', false)
    })
  },

  async actionFindOne({ commit, dispatch }, id) {
    dispatch('actionTriggerLoader', true)
    await findOne(id).then(res => {
      commit('GET_ONE_DEVELOPER', res)
      dispatch('actionTriggerLoader', false)
    })
  },

  async actionCreate({ dispatch }, input) {
    dispatch('actionTriggerLoader', true)
    await create(input).then(() => {
      dispatch('actionTriggerLoader', false)
    })
  },

  actionEdit({ commit }, input) {
    edit(input)
  },

  actionDelete({ commit }, id) {
    remove(id)
  }
}

const mutations = {
  [ALL_DEVELOPERS](state, data) {
    state.developers = data
  },
  [GET_ONE_DEVELOPER](state, data) {
    state.developer = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
