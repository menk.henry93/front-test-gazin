import axios from 'axios'
import store from '../store'

export default () => {
  const connection = axios.create({
    baseURL: process.env.VUE_APP_SERVICE_URL
  })

  connection.interceptors.request.use(
    function (config) {
      return config
    },
    function (error) {
      return Promise.reject(error)
    }
  )

  connection.interceptors.response.use(
    function (response) {
      return response
    },
    function (error) {
      store.dispatch('actionOpenSnackbar', {
        text: error.response.data.body
          ? error.response.data.body
          : error.response.data.message,
        color: 'red'
      })
      store.dispatch('actionTriggerLoader', false)
      return Promise.reject(error)
    }
  )

  return connection
}
