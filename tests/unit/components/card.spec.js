import Card from '../../../src/components/card'

describe('Component test', () => {
  it('Should be a object props in this component', () => {
    expect(typeof Card.props).toBe('object')
  })
})
