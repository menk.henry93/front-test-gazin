import { default as Toolbar } from '../../../src/components/toolbar'
import { mount } from '@vue/test-utils'

let wrapper = mount(Toolbar)
describe('Component test', () => {
  it('Should be a string props title in this component', () => {
    expect(typeof wrapper.props().title).toBe('string')
  })

  it('Should be a string props descriptionBtn in this component', () => {
    expect(typeof wrapper.props().descriptionBtn).toBe('string')
  })
})
