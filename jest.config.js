module.exports = {
  rootDir: '.',
  preset: '@vue/cli-plugin-unit-jest',
  collectCoverage: false,
  transformIgnorePatterns: ['\\node_modules\\@vuetify'],
  transform: {
    '^.+.vue$': 'vue-jest'
  }
}
