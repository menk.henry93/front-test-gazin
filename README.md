# Front Test Vue

This is experimental test with [Vue](https://vuejs.org/) and [Vuetify](https://vuetifyjs.com/en/). Simple presentation to Gazin test.

## Description

It is an application tha inserts new developer registrations, edits and removes. In other words, a CRUD.

## Clone the project

First of all you should download the project with this command:

```bash
git clone https://gitlab.com/menk.henry93/front-test-gazin.git
```

## Install the dependency

You need to install the dependencies.

Use NPM:

```bash
npm install
```

or YARN:

```bash
yarn
```

## Start project:

To run the application:

Use NPM:

```bash
npm run serve
```

or YARN:

```bash
yarn serve
```

## Start tests:

You could to run the tests too, with this commands:

If you use NPM:

```bash
npm run test:unit
```

or YARN:

```bash
yarn test:unit
```
